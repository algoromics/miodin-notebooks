Running Jupyter notebooks on Binder
========================

The notebooks contain miodin-based analysis workflows and can be run in Binder using the badge.
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/algoromics%2Fmiodin-notebooks/master)

NB: The vertical integration notebook is not able to run the factor analysis step on Binder due to memory restrictions.
To reproduce the results from this notebook it is recommended to run Jupyter locally.

Running locally
===============

To run the notebooks locally, first install [Docker](https://www.docker.com). In case you are running on Windows or Mac,
you may need to change Docker preferences to give containers more memory. To run the miodin notebooks, the memory limit
should be set to at least 4 GiB. Check [this](https://stackoverflow.com/questions/44533319/how-to-assign-more-memory-to-docker-container/44533437#44533437)
entry for how to adjust the Docker memory limit.

Next pull the miodin-notebook image from Docker Hub.

```
docker pull wolftower85/miodin-notebook:miodinpaper
```

Start a container using this command.

```
docker run -it -p 8888:8888 wolftower85/miodin-notebook:latest
```

Inside the container, run the following to start the Jupyter notebook app.

```
jupyter notebook --ip 0.0.0.0 --no-browser --allow-root
```

Finally, start the web browser and go to `localhost:8888`. The login token required is
given in the terminal output in the container.

```
Copy/paste this URL into your browser when you connect for the first time,
to login with a token:
    http://(ad47503c6743 or 127.0.0.1):8888/?token=45082bf5649501aa74774d5f1c75afa47f2109efb58a4310
```

