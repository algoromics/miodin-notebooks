# Based on the Jupyter Notebook R Stack https://github.com/jupyter/docker-stacks/tree/master/r-notebook
# <BinderCompatible>
FROM wolftower85/miodin-notebook:miodinpaper

MAINTAINER Benjamin Ulenborg <benjamin.ulfenborg@his.se>

USER $NB_USER

WORKDIR /home/jovyan

ADD . /home/jovyan

# USER root
# 
# RUN chmod a+w *.ipynb
# 
# USER $NB_USER
